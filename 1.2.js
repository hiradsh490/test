const generateRandomNumber = (_count) => {
  let numbers = [];
  for (let index = 0; index < _count; index++) {
    numbers.push(Math.floor(Math.random() * 100));
  }
  return numbers;
};

const sortList = (_array) => {
  return _array.sort((first, second) => first - second);
};

// async
const calculateData = (async () => {
  let numbers = await generateRandomNumber(10);
  let result = await sortList(numbers);

  console.log(result);
})();

// promise
const myPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Hi");
  }, 300);
});

myPromise
  .then(() => generateRandomNumber(10))
  .then((numbers) => sortList(numbers))
  .catch((error) => console.log(error));
